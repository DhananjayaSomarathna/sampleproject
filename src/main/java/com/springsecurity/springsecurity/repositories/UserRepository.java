package com.springsecurity.springsecurity.repositories;

import com.springsecurity.springsecurity.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface UserRepository extends JpaRepository<User,Long> {
    User findByUserName(String username);
}
