package com.springsecurity.springsecurity.service;

import com.springsecurity.springsecurity.domain.Role;
import com.springsecurity.springsecurity.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {

    User saveUser(User user);

    Role saveRole(Role role);

    void addRoleToUser(String username, String roleName);

    User getUser(String userName);

    List<User> getUser();
}
